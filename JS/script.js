"use strict";
const servicesList = document.querySelector(".services-list");
const servicesItemText = document.querySelectorAll(".service-item-text");
const serviceItem = document.querySelectorAll(".service-link");

servicesList.addEventListener("click", (evnt) => {
  serviceItem.forEach((el) => {
    el.classList.remove("active");
  });
  evnt.target.classList.add("active");

  servicesItemText.forEach((e) => {
    e.style.display = "none";
    if (e.dataset.content === evnt.target.dataset.category) {
      e.style.display = "flex";
      e.classList.add("active");
    }
  });
});

const amazingWorkList = document.querySelector(".amazing-work-list");
const amazingWorkCategory = document.querySelectorAll(".amazing-work-filter");
const amazingWorkProjects = document.querySelectorAll(
  ".our-amazing-work-projects"
);

amazingWorkList.addEventListener("click", (evnt) => {
  amazingWorkCategory.forEach((e) => {
    e.classList.remove("active");
  });
  evnt.target.classList.add("active");

  amazingWorkProjects.forEach((elem) => {
    elem.style.display = "none";
    if (elem.dataset.content === evnt.target.dataset.category) {
      elem.style.display = "block";
    } else if (evnt.target.dataset.category === "all") {
      elem.style.display = "block";
    }
  });
});

const feedBackAuthorBigPhoto = document.querySelector(".feedback-author-photo");
const authorList = document.querySelector(".author-list");
const current_icon = document.querySelector(".current-slide");
const next_sliderBttn = document.querySelector(".slider-button-right");
const prev_sliderBtnn = document.querySelector(".slider-button-left");

const slidBtn = document.querySelectorAll(".slider-button");

let fap = document.querySelector(".feedback-author-photo");

const feedbackCitations = [
  `Integer dignissim, augue tempus ultricies luctus, quam dui
  laoreet sem, non dictum odio nisi quis massa. Morbi pulvinar
  odio eget aliquam facilisis. Tempus ultricies luctus, quam dui
  laoreet sem, non dictum odio nisi quis massa. Morbi pulvinar
  odio eget aliquam facilisis.`,
  `Marin is extravagant, messy, and while quite mature, she's also
  clumsy. As a cosplayer and huge otaku, Marin is a big of fan of
  magical girl anime and adult video games. She strongly desires
  to cosplay as certain characters, seeing the notion of dressing
  up and becoming said characters as the ultimate form of love for
  them.`,
  `Madara Uchiha (うちはマダラ, Uchiha Madara) was the legendary
  leader of the Uchiha clan. He founded Konohagakure alongside his
  childhood friend and rival, Hashirama Senju, with the intention
  of beginning an era of peace. When the two couldn't agree on how
  to achieve that peace, they fought for control of the village, a
  conflict which ended in Madara's death. Madara, however, rewrote
  his death and went into hiding to work on his own plans.`,
  `Tanjiro Kamado (竈門かまど 炭たん治じ郎ろう Kamado Tanjirō?) is
  the protagonist of Demon Slayer: Kimetsu no Yaiba. He is a Demon
  Slayer in the Demon Slayer Corps, who joined to find a remedy to
  turn his sister, Nezuko Kamado, back into a human and to hunt
  down and kill demons, and later swore to defeat Muzan
  Kibutsuji,[6] the King of Demons, in order to prevent others
  from suffering the same fate as him.`,
  `Baki Hanma (範馬 刃牙, Hanma Baki) is the main character and
  protagonist of the Baki the Grappler franchise. At thirteen, he
  decided to take his training into his own hands to perform more
  intense training in his father's footsteps. He later aims to
  defeat him. Baki first fought in the no-rules arena at fifteen
  and went on to become its champion. He is the son of Yuujirou
  Hanma and Emi Akezawa and the half-brother of Jack Hanma.`,
  `
  Muhammad Avdol (モハメド・アヴドゥル Mohamedo Avuduru) is a core ally in Stardust Crusaders. Introduced as an Egyptian friend of Joseph Joestar, he joins the group on their journey to defeat DIO, providing knowledge and guidance about enemy Stand Users and local cultures along the way.`,
];
const feedbackNames = [
  `Hasan Ali`,
  `Marin Kitagawa`,
  `Madara Uchiha`,
  `Kamado Tanjiro`,
  `Baki Hanma`,
  `Muhamed Abdul`,
];
const feedbackJobs = [
  `UX Designer`,
  `My Dress Up Darling`,
  `Naruto`,
  `Demon Slayer`,
  `Baki Hanma`,
  `JOJO`,
];

const moveSliderLeft = (index) => {
  document.querySelectorAll(".authors-icons").forEach((elem) => {
    elem.classList.remove("current-slide");
  });
  index--;
  document
    .querySelectorAll(".authors-icons")
    [index].classList.add("current-slide");

  let imgSrc = document.querySelector(".current-slide").children[0].src;
  fap.src = imgSrc;
  document.querySelector(".feedback-text").textContent =
    feedbackCitations[index];
  document.querySelector(".feedback-author").textContent = feedbackNames[index];
  document.querySelector(".feedback-author-job").textContent =
    feedbackJobs[index];
};

const moveSliderRight = (index) => {
  document.querySelectorAll(".authors-icons").forEach((elem) => {
    elem.classList.remove("current-slide");
  });
  index++;
  document
    .querySelectorAll(".authors-icons")
    [index].classList.add("current-slide");

  let imgSrc = document.querySelector(".current-slide").children[0].src;
  fap.src = imgSrc;
  document.querySelector(".feedback-text").textContent =
    feedbackCitations[index];
  document.querySelector(".feedback-author").textContent = feedbackNames[index];
  document.querySelector(".feedback-author-job").textContent =
    feedbackJobs[index];
};

slidBtn.forEach((btn, index) => {
  btn.addEventListener("click", () => {
    btn.classList.add("anim");
    setTimeout(() => {
      btn.classList.remove("anim");
    }, 200);

    let counter;
    document.querySelectorAll(".authors-icons").forEach((elem, indx) => {
      if (elem.classList.contains("current-slide")) {
        counter = indx;
      }
    });

    if (index === 1) {
      if (counter > 2) {
        if (
          !document
            .querySelector(".authors-icons-hidden")
            .hasAttribute("hidden")
        ) {
          if (counter > 4) {
            return;
          } else {
            moveSliderRight(counter);
          }
        } else {
          for (let i = 0; i < 3; i++) {
            document
              .querySelectorAll(".authors-icons")
              [i].classList.add("hide");
          }
          setTimeout(() => {
            for (let i = 0; i < 3; i++) {
              document
                .querySelectorAll(".authors-icons")
                [i].setAttribute("hidden", true);
            }
            document.querySelectorAll(".authors-icons-hidden").forEach((e) => {
              e.removeAttribute("hidden");
              e.classList.add("animation-appear");
            });
            setTimeout(() => {
              document
                .querySelectorAll(".authors-icons-hidden")
                .forEach((e) => {
                  e.classList.remove("animation-appear");
                });
            }, 300);
          }, 300);
        }
      } else {
        moveSliderRight(counter);
      }
    } else {
      if (counter < 1) {
        return;
      } else if (counter < 4) {
        if (!document.querySelector(".authors-icons").hasAttribute("hidden")) {
          if (counter < 1) {
            return;
          } else {
            moveSliderLeft(counter);
          }
        } else {
          document.querySelectorAll(".authors-icons-hidden").forEach((elem) => {
            elem.classList.add("animation-dissap");
          });
          setTimeout(() => {
            document
              .querySelectorAll(".authors-icons-hidden")
              .forEach((elem) => {
                elem.classList.remove("animation-dissap");
                elem.setAttribute("hidden", true);
              });
            for (let i = 0; i < 3; i++) {
              document
                .querySelectorAll(".authors-icons")
                [i].removeAttribute("hidden");
            }
            for (let i = 0; i < 3; i++) {
              document
                .querySelectorAll(".authors-icons")
                [i].classList.replace("hide", "not-hidden");
            }
            setTimeout(() => {
              for (let i = 0; i < 3; i++) {
                document
                  .querySelectorAll(".authors-icons")
                  [i].classList.remove("not-hidden");
              }
            }, 300);
          }, 300);
        }
      } else {
        moveSliderLeft(counter);
        console.log(counter);
      }
    }
  });
});

const authorIcons = document.querySelectorAll(".authors-icons");

authorList.addEventListener("click", (evnt) => {
  if (!evnt.target.closest(".authors-icons")) {
    return;
  }
  let target = evnt.target.closest(".authors-icons");
  authorIcons.forEach((el) => {
    el.classList.remove("current-slide");
  });
  let counter;
  target.classList.add("current-slide");

  document.querySelectorAll(".authors-icons").forEach((elem, indx) => {
    if (elem.classList.contains("current-slide")) {
      counter = indx;
    }
  });

  let imgSrc = target.children[0].src;

  document.querySelector(".feedback-author-photo").src = imgSrc;

  document.querySelector(".feedback-text").textContent =
    feedbackCitations[counter];
  document.querySelector(".feedback-author").textContent =
    feedbackNames[counter];
  document.querySelector(".feedback-author-job").textContent =
    feedbackJobs[counter];
});
